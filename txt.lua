local citations = {}

local indentSize = 4
local lineLength = 80

local inspect = require("inspect")

-- @from https://gist.github.com/jaredallard/ddb152179831dd23b230, thank you!
local function Split(s, delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( s, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( s, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( s, delimiter, from  )
  end
  table.insert( result, string.sub( s, from  ) )
  return result
end

-- Prints the given warning to the console
local function Warning(s)
  print("Warning: " .. s)
end

-- Returns a padding string for the given level
local function Indent(level)
  return string.rep(" ", indentSize * level)
end

-- Removes all spaces or newlines from the start and end of
-- the given string and returns the result
local function Trim(text)
  -- see https://stackoverflow.com/a/51181334
  return text:gsub('^%s*(.-)%s*$', '%1')
end

-- Returns the given text centered using spaces based on 
-- the global line width variable
local function Center(text)
  local textLength = text:len()

  if textLength >= lineLength then
    -- The text cannot be centered, since it is already
    -- longer than or just as long as the lineLength
    return text
  end

  local numSpacesRequired = math.floor(lineLength / 2) - math.floor(textLength / 2)

  return string.rep(" ", numSpacesRequired) .. text
end

-- Splits the given input line based on the maxLength parameter
local function SplitLine(inputLine, maxLength)
  local lines = { "" }

  for word in inputLine:gmatch("%S+") do
    local lineLen = lines[#lines]:len()

    if word:len() + lineLen > maxLength and lineLen > 0 then
      table.insert(lines, "")
    end

    lines[#lines] = lines[#lines] .. word .. " "
  end

  return lines
end

-- Renders the given text for the given indentation level.
-- Automatically inserts newlines when necessary at the
-- correct position and indents the new lines accordingly
local function TextBlock(text, level)
  local lines = {}

  for _, line in pairs(Split(text, "\n")) do
    local subLines = SplitLine(line, lineLength - level * indentSize)

    for _, subLine in pairs(subLines) do
      table.insert(lines, subLine)
    end
  end

  return table.concat(lines, "\n" .. Indent(level))
end

-- This function is called once for the entire
-- document and enables altering of the document
-- body at the end of the rendering phase
function Doc(body, metadata, variables)
  local bibliographyLines = {}
  -- Include any citations at the bottom of the document
  for _, citation in pairs(citations) do
    table.insert(bibliographyLines, Indent(1) .. "[" .. citation.citationId .. "]: " .. Trim(citation.citationSuffix))
  end

  return body .. "\n" .. table.concat(bibliographyLines, "\n")
end

function Blocksep()
  return "\n\n"
end

function Str(text)
  return Trim(text)
end

function Space()
  return " "
end

function SoftBreak()
  return " "
end

function LineBreak()
  return "\n"
end

function Emph(text)
  Warning("emphasised text is not supported and will be discarded")
  return text
end

function Strong(text)
  Warning("bold text is not supported and will be discarded")
  return text
end

function Subscript(text)
  return "_" .. text
end

function Superscript(text)
  return "^" .. text
end

function SmallCaps(text)
  return string.upper(text)
end

function Strikeout(text)
  return "~" .. text .. "~"
end

function Link(text, target)
  return text .. " (<" .. target .. ">)"
end

function Image()
  Warning("inline images are not supported and will not be shown")
  -- The image is rendered as two newlines, to denote the place
  -- where the image would have been
  return "\n\n"
end

function Code(code)
  return "`" .. code .. "`"
end

function InlineMath(text)
  Warning("inline math is not supported and the raw TeX commands will be shown")
  return text
end

function DisplayMath(text)
  Warning("inline math is not supported and the raw TeX commands will be shown")
  return text
end

function SingleQuoted(text)
  return "'" .. text .. "'"
end

function DoubleQuoted(text)
  return "\"" .. text .. "\""
end

function Note(text)
  return Indent(2) .. TextBlock(text, 2)
end

function Span(text)
  return Indent(1) .. TextBlock(text, 1)
end

function RawInline(format, text)
  return Indent(1) .. TextBlock(text, 1)
end

function Cite(s, cs)
  local citation = cs[1]

  local isReferentialCitation = citation.citationMode   == "AuthorInText" or citation.citationSuffix == ""
  if isReferentialCitation then
    -- The citation is simply a reference to a previous full citation
    if citations[citation.citationId] == nil then
      Warning("the citation '" .. citation.citationId .. "' does not exist")
      return ""
    end
  else
    -- The citation is new; add it to the table of citations
    citations[citation.citationId] = citation
  end

  local isHidden = Trim(citation.citationPrefix) == "hide" or Trim(citation.citationPrefix) == "hidden"
  if isHidden then
    return ""
  end

  return "[" .. citation.citationId .. "]"
end

function Plain(text)
  return text
end

function Para(text)
  return Indent(1) .. TextBlock(text, 1)
end

function Header(level, text)
  if level == 1 then
    return Center(text)
  end

  return text
end

function BlockQuote(text)
  return TextBlock(text, 1)
end

function HorizontalRule()
  return Center("---")
end

function LineBlock(lines)
  return "foo" -- TODO
end

function CodeBlock(text, attr)
  return text
end

function BulletList(items)
  local buffer = {}

  for _, item in pairs(items) do
    local bullet = "*" .. string.rep(" ", indentSize - 1)
    local text = Indent(1) .. bullet .. TextBlock(item, 2)
    table.insert(buffer, text)
  end

  return table.concat(buffer, "\n")
end

function OrderedList(items)
  local buffer = {}
  
  for index, item in pairs(items) do
    local listIndex = tostring(index)
    local numIndentSpaces = math.max(1, indentSize - listIndex:len() - 1)
    local bullet = listIndex .. ")" .. string.rep(" ", numIndentSpaces)
    local text = Indent(1) .. bullet .. TextBlock(item, 2)
    table.insert(buffer, text)
  end

  return table.concat(buffer, "\n")
end

function DefinitionList(items)
  return table.concat(items, "\n")
end

function CaptionedImage(src, tit, caption, attr)
  Warning("inline images are not supported and will not be shown")
  return ""
end

function Table(caption, aligns, widths, headers, rows)
  return ""
end

function RawBlock(format, str)
  return str
end

function Div(s, attr)
  return s
end
